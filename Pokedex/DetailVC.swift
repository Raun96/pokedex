//  
//  DetailVC.swift
//  Pokedex
//
//  Created by Raunak Singh on 25/9/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    var poke: Pokemon!


    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var defenseLbl: UILabel!
    @IBOutlet weak var pokeIDLbl: UILabel!
    @IBOutlet weak var attackLbl: UILabel!
    @IBOutlet weak var evoLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var currentEvoImg: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        poke.downloadData {
            self.updateUI()
            
        }
        
    }

    func updateUI() {
        mainImg.image = UIImage(named: "\(poke.pokemonID)")
        currentEvoImg.image = UIImage(named: "\(poke.pokemonID)")
        nameLbl.text = poke.name.capitalized
        heightLbl.text = poke.height
        defenseLbl.text = poke.defense
        attackLbl.text = poke.attack
        pokeIDLbl.text = "\(poke.pokemonID)"
        weightLbl.text = poke.weight
        typeLbl.text = poke.type
        descriptionLbl.text = poke.mainDescription
        
    }
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

//
//  Constants.swift
//  Pokedex
//
//  Created by Raunak Singh on 30/9/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import Foundation

let BASE_URL = "http://pokeapi.co"
let POKE_URL = "/api/v1/pokemon/"

typealias DownloadCompleted = () -> ()


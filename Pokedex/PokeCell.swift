//
//  PokeCell.swift
//  Pokedex
//
//  Created by Raunak Singh on 14/9/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
    

    
    @IBOutlet weak var pokeImage: UIImageView!
    
    @IBOutlet weak var pokeLabel: UILabel!
    
    var pokemon: Pokemon!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 5.0
        
    }
    
    func configCell(pokemon: Pokemon){
        self.pokemon = pokemon
        pokeLabel.text = self.pokemon.name.capitalized
        pokeImage.image = UIImage(named: "\(self.pokemon.pokemonID)")
    }
}


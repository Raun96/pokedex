//
//  Pokemon.swift
//  Pokedex
//
//  Created by Raunak Singh on 13/9/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import Foundation
import Alamofire

class Pokemon {
    
    private var _name: String!
    private var _pokemonID: Int!
    private var _type: String!
    private var _description: String!
    private var _height: String!
    private var _weight: String!
    private var _attack: String!
    private var _defense:String!
    private var _evolution: String!
    private var pokeURL: String!
    
    
     var name: String {
        return _name
    }
     var pokemonID: Int{
       return _pokemonID
    }
    var type: String {
        if _type == nil{
            _type = ""
        }
        return _type
    }
    var mainDescription: String {
        if _description == nil {
            _description = ""
        }
        return _description
    }
    var height: String {
        if _height == nil{
            _height = ""
        }
        return _height
    }
    var weight: String {
        if _weight == nil {
            _weight = ""
        }
        return _weight
    }
    var attack: String{
        if _attack == nil {
            _attack = ""
        }
        return _attack
    }
    var defense: String{
        if _defense == nil {
            _defense = ""
        }
        return _defense
    }
    var evolution: String{
        if _evolution == nil {
            _evolution = ""
        }
        return _evolution
    }
    
    
    init(name: String, id: Int) {
        self._name = name
        self._pokemonID = id
        self.pokeURL = "\(BASE_URL)\(POKE_URL)\(self._pokemonID!)/"
    }
    
    func downloadData(completed: @escaping DownloadCompleted){
        print(self.pokeURL)
        Alamofire.request(pokeURL).responseJSON { response in
            let result = response.result.value
            if let dict = result as? Dictionary<String, AnyObject> {
                if let attack = dict["attack"] as? Int {
                    self._attack = "\(attack)"
                    print(self._attack)
                }
                if let defense = dict["defense"] as? Int {
                    self._defense = "\(defense)"
                    print(self._defense)
                }
                if let weight = dict["weight"] as? String {
                    self._weight = weight
                    print(self._weight)
                }
                if let height = dict["height"] as? String {
                    self._height = height
                    print(self._height)
                }
                
                if let types = dict["types"] as? [Dictionary<String,AnyObject>], types.count > 0 {
                    
                    if let name = types[0]["name"] as? String {
                        self._type = name.capitalized
                    }
                    if types.count > 1  {
                        for x in 1..<types.count {
                            if let name = types[x]["name"] as? String {
                                self._type! += "/\(name.capitalized)"
                            }
                        }
                    }
                } else {
                    self._type = ""
            }
            
                if let desc = dict["descriptions"] as? [Dictionary<String,AnyObject>], desc.count > 0 {
                    let url: String = desc[0]["resource_uri"] as! String
                    let descURL = BASE_URL+url
                    print("count = \(desc.count)")
                    Alamofire.request(descURL).responseJSON { response in
                        if let descDict = response.result.value as? Dictionary<String, AnyObject> {
                            if let description = descDict["description"] as? String{
                               let newDescription = description.replacingOccurrences(of: "POKMON", with: "pokemon")
                                self._description = newDescription
                            }
                        }
                        print(self._description)
                    }
                }
               
            
            completed()
        }
        
    }
}
}



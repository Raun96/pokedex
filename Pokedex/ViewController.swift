//
//  ViewController.swift
//  Pokedex
//
//  Created by Raunak Singh on 13/9/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var pokeArray: [Pokemon] = []
    var filteredPokeArray = [Pokemon]()
    var inSearchMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        searchBar.delegate = self
        parseCSV()
        //To cancel keyboard with a tap anywhere on the screen. And still registering taps on TableViews and CollectionViews.
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }

    func parseCSV() {                           ///////////////////// To parse through the CSV file.
        let path = Bundle.main.path(forResource: "pokemon", ofType: "csv")!
        do {
            let csv = try CSV(contentsOfURL: path)
            let rows = csv.rows
            
            for row in rows {
                let pokeID = Int(row["id"]!)!
                let name = row["identifier"]!
                let poke = Pokemon(name: name, id: pokeID)
                pokeArray.append(poke)
                
            }
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pokeCell" , for: indexPath) as? PokeCell{
            let poke: Pokemon!
            
            if inSearchMode == false{
            poke = pokeArray[indexPath.row]
            cell.configCell(pokemon: poke)
            } else {
                poke = filteredPokeArray[indexPath.row]
                cell.configCell(pokemon: poke)
            }
            return cell
        }
        else{
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //when a cell is selected.
        var poke: Pokemon!
        if inSearchMode {
            poke = filteredPokeArray[indexPath.row]
        } else {
            poke = pokeArray[indexPath.row]
        }
        performSegue(withIdentifier: "showDetailVC", sender: poke)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if inSearchMode == false {
            return pokeArray.count
        } else {
            return filteredPokeArray.count
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 90)            //define the size of a cell.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            inSearchMode = false
            collectionView.reloadData()
            
        } else {
            inSearchMode = true
            let searchText = searchBar.text?.lowercased()
            
            filteredPokeArray = pokeArray.filter({$0.name.range(of: searchText!) != nil})
            collectionView.reloadData()
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailVC" {
            if  let detailsVC = segue.destination as? DetailVC {
                if let poke =  sender as? Pokemon {
                    detailsVC.poke = poke
                }
            }
        }
    }
    
}

